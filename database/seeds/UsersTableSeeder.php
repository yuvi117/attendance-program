<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role' 	  			=> 1,
            'status'            => 1,
            'name' 	   			=> 'Admin',
            'email'    			=> 'admin@admin.com',
            'password' 			=> bcrypt('123456'),
            'email_verified_at' => Carbon::now(),
            'updated_at'		=> Carbon::now(),
            'created_at' 		=> Carbon::now(),
        ]);
    }
}
