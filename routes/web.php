<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('/unverified', function () {
    return view('unverified');
})->name('unverified');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('logout', 'Profile\LoginController@logout')->name('logout');

Route::group(['namespace' => 'Profile', 'middleware' => ['check.login']], function(){

	Route::get('/register', 'RegisterController@showRegistrationForm')->name('register');
	Route::post('/register', 'RegisterController@register')->name('register');
    Route::get('login', 'LoginController@showLoginForm')->name('LoginForm');
    Route::post('login', 'LoginController@login')->name('login');
    
	Route::get('/verify-account/{slug}', 'ProfileController@verifyAccount')->name('verifyAccount');
});

/*Admin Panel*/
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.'], function(){

    Route::group(['middleware' => ['auth', 'admin']], function(){
    	
    	Route::get('/', 'AttendanceController@allAttendance')->name('index');
    	//Route::get('/', function () {
		//    return view('admin.index');
		//})->name('index');
 	});
});

/*User Panel*/
Route::group(['namespace' => 'Student', 'middleware' => ['auth', 'verified']], function(){

	Route::post('mark-attendance', 'AttendanceController@markAttendance')->name('markAttendance');
});
