<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $fillable = [
    	'user_id',
    	'started_at',
    ];

    /*
    |--------------------------------------------------------------------------
    | Query Scopes
    |--------------------------------------------------------------------------
    */
    public function scopeData($query, $data)
    {
       return $query->where($data);
    }

    public function scopeInBetween($query, $startDate, $endDate)
    {
       return $query->where('started_at', '>=', $startDate)->where('started_at', '<=', $endDate);
    }

    /*
    |--------------------------------------------------------------------------
    | Eloquent Relations
    |--------------------------------------------------------------------------
    */
    public function user()
    { 
       return $this->belongsTo(User::class);
    }
}
