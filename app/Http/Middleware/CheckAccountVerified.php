<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use App\Support\Responses\FlashAndRedirectResponse;

class CheckAccountVerified
{
    use FlashAndRedirectResponse;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->status == 0){
            return $this->failRedirect(route('unverified'), 'Please verify your email before login');
        }

        return $next($request);
    }
}
