<?php

namespace App\Http\Middleware;

use Closure;
use App\Support\Responses\FlashAndRedirectResponse;

class CheckAdmin
{
    use FlashAndRedirectResponse;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();

        if($user){

            if(!($user->role == 1)){

                return $this->failRedirect(route('home'), 'You don\'t have Admin Access');
            }
        }

        return $next($request);
    }
}
