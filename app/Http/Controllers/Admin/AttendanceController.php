<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Carbon\Carbon;
use App\Attendance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AttendanceController extends Controller
{
    public function allAttendance(Request $request)
    {
        if($request->start_date && $request->end_date){

            $rules = [
                'start_date' => 'required|date_format:d-m-Y',
                'end_date'   => 'required|date_format:d-m-Y',
            ];

            $messages = [
                'start_date.date_format' => 'The Date format should be like \'yyyy-mm-dd\'',
                'end_date.date_format' => 'The Date format should be like \'yyyy-mm-dd\'',
            ];

            $this->validate($request, $rules, $messages);

    		$startDate = Carbon::parse($request->start_date)->copy()->startOfDay();
    		$endDate = Carbon::parse($request->end_date)->copy()->endOfDay();
    	} else{
    		$startDate = Carbon::now()->copy()->startOfDay();
    		$endDate = Carbon::now()->copy()->endOfDay();
    	}

    	//$datePeriod = $this->datePeriod(Carbon::parse('12-11-2018'), Carbon::parse('18-11-2018'));
    	$datePeriod = $this->datePeriod($startDate, $endDate);

    	foreach ($datePeriod as $value) {
             $data[] = Attendance::inBetween(\Carbon\Carbon::parse($value->format('d-m-Y'))->copy()->startOfDay(), \Carbon\Carbon::parse($value->format('d-m-Y'))->copy()->endOfDay())->with('user')->get()->groupBy('user_id')->toArray();
        }

    	$attendances = Attendance::inBetween($startDate, $endDate)->with('user')->get()->groupBy('user_id');

    	$users = User::whereStatus(1)->whereRole(2)->with('attendances')->get();

    	return view('admin.index',compact('attendances', 'users', 'datePeriod'));
    }

    public function datePeriod($startDate, $endDate, $daysmargin = '+0 day')
    {
        $period = new \DatePeriod(
                  new \DateTime($startDate),
                  new \DateInterval('P1D'),
                  new \DateTime($endDate . $daysmargin)
        );
        return $period;
    }
}
