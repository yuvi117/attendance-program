<?php

namespace App\Http\Controllers\Student;

use Carbon\Carbon;
use App\Attendance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AttendanceController extends Controller
{
    public function markAttendance(Request $request)
    {
    	$today = Carbon::now();
    	$attendance = Attendance::inBetween($today->copy()->startOfDay(), $today->copy()->endOfDay())->data(['user_id' => auth()->user()->id])->first();

    	if($attendance){

    		return $this->failRedirect(route('home'), 'You have already marked your attendance for the day');
    	}

    	$data = [
    		'user_id' 	 => auth()->user()->id,
    		'started_at' => Carbon::now(),
    	];

    	Attendance::Create($data);

    	return $this->successRedirect(route('home'), 'Attendance has been marked successfully');
    }

    public function myAttendance($startDate = null, $endDate = null)
    {
    	if($startDate == null || $endDate = null){

    		$startDate = Carbon::now()->copy()->startOfDay();
    		$endDate = Carbon::now()->copy()->endOfDay();
    	}
    	$attendances = Attendance::inBetween($startDate, $endDate)->get();

    	return view('student.attendance',compact('attendances'));
    }
}
