<?php

namespace App\Http\Controllers\Profile;

use DB;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function verifyAccount($slug)
    {
    	$tokenData = DB::table('password_resets')
                ->whereToken($slug)->first();

        if(!$tokenData){
            $content = "Invalid Token Please Regenerate.";
            return view('feedback', compact('content'));
        }
        
        if(Carbon::now() > Carbon::parse($tokenData->created_at)->addMinutes(30)){
            $content = "Token Expired Please Regenerate.";
            return view('feedback', compact('content'));
        }

        $user = User::whereEmail($tokenData->email)->first();
        $user->email_verified_at = Carbon::now();
        $user->status = 1;
        $user->save();

       	DB::table('password_resets')->whereToken($slug)->delete();

        return $this->successRedirect(route('login'), 'Email has been verified successfully');
    }
}