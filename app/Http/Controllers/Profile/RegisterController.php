<?php

namespace App\Http\Controllers\Profile;

use DB;
use App\User;
use Carbon\Carbon;
use App\Mail\VerifyEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
    	$rules = [
            'name' 		=> ['required', 'string', 'max:255'],
            'email' 	=> ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' 	=> ['required', 'string', 'min:6', 'confirmed'],
        ];

        $data = $this->validate($request, $rules);

        $data['password'] = Hash::make($data['password']);

        $user = User::create($data);

        $str = str_random(45);

        DB::table('password_resets')->insert([
            'email' 	  		=> $user->email,
            'token' 	   		=> $str,
            'created_at' 		=> Carbon::now(),
        ]);

        $link = url('verify-account/'.$str);

        Mail::to($user->email)->send(new VerifyEmail($user, $link));

        return $this->successRedirect(route('login'), 'You has been register successfully, Please verify you email before login');
    }
}
