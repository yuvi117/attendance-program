<?php

namespace App\Http\Controllers\Profile;

//use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {   
        $rules = [
            'email'     => 'required|email|exists:users,email',
            'password'  => 'required|min:6',
        ];

        $messages = [
            'email.exists' => 'Please enter registered email address',
        ];

        $data = $this->validate($request, $rules, $messages);

        /*$user = User::whereEmail($data['email'])->first();

        if($user->status == 0){
            return $this->failRedirect(route('login'), 'Please verify your email before login');
        }
*/
        if(auth()->guard()->attempt($data)){

            if(auth()->user()->role == 1){

                return redirect()->route('admin.index');

            }
            return redirect()->route('home');

        } else {
            return $this->failRedirect(route('login'), 'Invalid Email or Password');
        }

    }

    public function logout(Request $request)
    {
        auth()->guard()->logout();

        $request->session()->invalidate();

        return redirect()->route('login');
    }
}
