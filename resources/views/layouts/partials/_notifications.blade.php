@if($errors->any())
    <div class="row alert_msg">
        <div class="alert alert-danger col-md-6 col-md-offset-3 text-center">
            <button class="close" data-close="alert"></button>
            <span>{!! $errors->first() !!}</span>
        </div>
        <div class="clearfix"></div>
    </div>
@endif

@if(Session::has('notification'))
    <div class="row alert_msg">
        <div class="alert alert-{{ Session::get('notification')['status'] == 'success' ? 'success' : 'danger'}} col-md-6 col-md-offset-3 text-center alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <span>{!! Session::get('notification')['message'] !!}</span>
        </div>
        <div class="clearfix"></div>
    </div>
@endif

<script type="text/javascript">
    
    setTimeout(function() {
        $(".alert_msg").hide();
    }, 4000);

</script>